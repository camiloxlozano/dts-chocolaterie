<?php
include '../db/conn.php';
session_start();
if ($_SESSION['user'] == null ||  $_SESSION['user'] == ''){
	header("Location: index.php");
	}else{
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>LISTE DE COMMANDES</title>

    <!-- Normalize V8.0.1 -->
    <link rel="stylesheet" href="./css/normalize.css">

    <!-- Bootstrap V4.3 -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">

    <!-- Bootstrap Material Design V4.0 -->
    <link rel="stylesheet" href="./css/bootstrap-material-design.min.css">

    <!-- Font Awesome V5.9.0 -->
    <link rel="stylesheet" href="./css/all.css">

    <!-- Sweet Alerts V8.13.0 CSS file -->
    <link rel="stylesheet" href="./css/sweetalert2.min.css">

    <!-- Sweet Alert V8.13.0 JS file-->
    <script src="./js/sweetalert2.min.js"></script>

    <!-- jQuery Custom Content Scroller V3.1.5 -->
    <link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">

    <!-- General Styles -->
    <link rel="stylesheet" href="./css/style.css">


</head>


<body>
    <!-- Main container -->
    <main class="full-box main-container">
        		<!--Menu-->
		<?php
		include 'includes/menu.php';
		?>

        <section class="full-box page-content">
            <nav class="full-box navbar-info">
                <a href="#" class="float-left show-nav-lateral">
                    <i class="fas fa-exchange-alt"></i>
                </a>
                <a href="command-update.php">
                    <i class="fas fa-user-cog"></i>
                </a>
                <a href="#" class="btn-exit-system">
                    <i class="fas fa-power-off"></i>
                </a>
            </nav>
            <!-- Page header -->
            <div class="full-box page-header">
                <h3 class="text-left">
                    <i class="fas fa-clipboard-list fa-fw"></i> &nbsp; LISTE DE COMMANDES
                </h3>
                <p class="text-justify">
				Vous pouvez voir ici la liste des commandes effectuées par les clients
				                </p>
            </div>
            
            <!--CONTENT-->
            
			 <div class="container-fluid">
				<div class="table-responsive">
					<table class="table table-dark table-sm">
						<thead>
							<tr class="text-center roboto-medium">
								<th>#</th>
								<th>NOM DU CLIENT</th>
								<th>DATE</th>
								<th>COMMENTAIRE</th>
								<th>TELEPHONE</th>
								<th>PRODUITS</th>
								<th>EFFACER</th>
							</tr>
						</thead>
						<tbody>

						<?php
						$sql = "SELECT * FROM commandes";
							$result = $conn->query($sql);
							if ($result->num_rows > 0) {
								// output data of each row
								while($row = $result->fetch_assoc()) {
						?>
							<tr class="text-center" >
								<td><?php echo $row["id"]; ?></td>
								<td><?php echo $row["Nom"]; ?></td>
								<td><?php echo $row["DATE"]; ?></td>
								<td><?php echo $row["commentaire"]; ?></td>
								<td><?php echo $row["Telephone"]; ?></td>
								<td>
								<?php echo $row["produits"]; ?>
								</td>
								<td>
									<form action="../db/delete_commande.php?id=<?php echo $row["id"]; ?>" method="POST">
									<button type="submit" name="update_list" class="btn btn-warning">
		  								<i class="far fa-trash-alt"></i>
									</button>
									</form>
								</td>
							</tr>
								<?php } } ?>
						</tbody>
					</table>
				</div>

			</div>
        </section>
    </main>
    
    	
	<!--=============================================
	=            Include JavaScript files           =
	==============================================-->
	<!-- jQuery V3.4.1 -->
	<script src="./js/jquery-3.4.1.min.js" ></script>

	<!-- popper -->
	<script src="./js/popper.min.js" ></script>

	<!-- Bootstrap V4.3 -->
	<script src="./js/bootstrap.min.js" ></script>

	<!-- jQuery Custom Content Scroller V3.1.5 -->
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>

	<!-- Bootstrap Material Design V4.0 -->
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>

	<script src="./js/main.js" ></script>
</body>
</html>