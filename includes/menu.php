 <!--Navbar-->
 <nav class="navbar navbar-expand-lg navbar-light bg-dark fixed-top pt-3 ">
    <div class="container py-0">
    <a class="navbar-brand text-light" href="index.php">El logo</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav ml-auto f-abel ">
        <li class="nav-item mr-3 cool-link">
          <a class="nav-link active" href="index.php">Accueil <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item mr-3 cool-link text-light">
          <a class="nav-link text-light" href="index.php#Evenements">Evénements</a>
        </li>
        <li class="nav-item mr-3 cool-link text-light">
          <a class="nav-link text-light" href="index.php#apropos">À propos</a>
        </li>
        <li class="nav-item dropdown  cool-link text-light">
          <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            BOUTIQUE
          </a>
          <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item bg-dark text-white" href="boutique.php#produits">Tout</a>
            <a class="dropdown-item bg-dark text-white" href="boutique.php?type=chocolat#produits">Chocolaterie</a>
            <a class="dropdown-item bg-dark text-white" href="boutique.php?type=patisserie#produits">Pâtisserie</a>
            <a class="dropdown-item bg-dark text-white" href="boutique.php?type=glace#produits">Glacerie</a>
            <a class="dropdown-item bg-dark text-white" href="boutique.php?type=gourmandise#produits">Gourmandise</a>

          </div>
        </li>
        <li class="nav-item mt-2 ml-4">
          <a href="panier.php">
          <i class="fas fa-cart-plus text-light mr-2 mt-1"></i>
          <span class="text-light">
         <?php echo (empty($_SESSION['CARRITO']))?0:count($_SESSION['CARRITO']); ?>
          </span>
          </a>

        </li>

      </ul>
    </div>
  </div>
</nav>
