	<!-- Main container -->
	<main class="full-box main-container">

		<!-- Nav lateral -->
		<section class="full-box nav-lateral">
			<div class="full-box nav-lateral-bg show-nav-lateral"></div>
			<div class="full-box nav-lateral-content">
				<figure class="full-box nav-lateral-avatar">
					<i class="far fa-times-circle show-nav-lateral"></i>
					<img src="./assets/avatar/Avatar.png" class="img-fluid" alt="Avatar">
					<figcaption class="roboto-medium text-center">
						User <br><small class="roboto-condensed-light">Admin</small>
					</figcaption>
				</figure>
				<div class="full-box nav-lateral-bar"></div>
				<nav class="full-box nav-lateral-menu">
					<ul>
						<li>
							<a href="home.php"><i class="fab fa-dashcube fa-fw"></i> &nbsp; Dashboard</a>
						</li>

						<li>
							<a href="#" class="nav-btn-submenu"><i class="fas fa-pallet fa-fw"></i> &nbsp; Produits <i class="fas fa-chevron-down"></i></a>
							<ul>
							<li>
									<a href="produit-list.php"><i class="fas fa-clipboard-list fa-fw"></i> &nbsp; Liste des Produits</a>
								</li>
								<li>
									<a href="new-chocolat.php"><i class="fas fa-plus fa-fw"></i> &nbsp; Chocolats</a>
								</li>

								<li>
									<a href="new-patisserie.php"><i class="fas fa-plus fa-fw"></i> &nbsp; Patisserie</a>
								</li>

								<li>
									<a href="new-glace.php"><i class="fas fa-plus fa-fw"></i> &nbsp; Glaces</a>
								</li>

								<li>
									<a href="new-gourmandise.php"><i class="fas fa-plus fa-fw"></i> &nbsp; Gourmandise</a>
							</li>
									<!-- Button trigger modal -->
<a style="color:white;" data-toggle="modal" data-target="#exampleModal">
	<i class="fas fa-redo"></i> &nbsp; Changer la legend des chocolats
</a>


								</li>

							</ul>
						</li>


						<li>
							<a href="#" class="nav-btn-submenu"><i class="fas fa-users fa-fw"></i> &nbsp; Evenements<i class="fas fa-chevron-down"></i></a>
							<ul>
								<li>
									<a href="evenement-new.php"><i class="fas fa-plus fa-fw"></i> &nbsp; Ajouter un evenement</a>
								</li>
								<li>
									<a href="evenement-list.php"><i class="fas fa-clipboard-list fa-fw"></i> &nbsp; Liste des evenement</a>
								</li>

							</ul>
						</li>

						<li>
								<a href="#" class="nav-btn-submenu"><i class="fas fa-clipboard-list"></i> &nbsp; Commandes <i class="fas fa-chevron-down"></i></a>
								<ul>

									<li>
										<a href="command-list.php"><i class="fas fa-clipboard-list fa-fw"></i> &nbsp; Liste des commandes</a>
									</li>

								</ul>
							</li>
						

						<li>
							<a href="company.php"><i class="fas fa-store-alt fa-fw"></i> &nbsp; Entreprise</a>
						</li>
						
					</ul>
				</nav>
			</div>
			<!-- Modal -->
	
		</section>

		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouvelle Legende</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <form action="#" id="urlform" method="post">
		<label>Url de la photo</label>
		<input type="url" name="urllegende"  class="form-control" autocomplete="off" required>
		</div>
		<div class="modal-footer">
			<button class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary">Save changes</button>
		</form>

      </div>
    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
