<?php
include 'db/conn.php';
session_start();

$sql = "SELECT * FROM enterprise WHERE id= 1";
$info = $conn->query($sql);
$enterprise = $info->fetch_assoc();
?>

<!doctype html>
<html class="no-js" lang="fr">

<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->
  <!--OWL Carrousel-->
  <link rel="stylesheet" href="css/owl.carousel.css">
  <link rel="stylesheet" href="css/owl.theme.default.css">
  <!--Boostrap-->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/main.css">
  <!--animated CSS-->
  <link rel="stylesheet" href="css/animate.css">
  <!--font Awesome-->
  <script src="https://kit.fontawesome.com/5fe0cb84c4.js" crossorigin="anonymous"></script>
  <!--Google font-->
  <link href="https://fonts.googleapis.com/css?family=Abel|Jomolhari|Roboto&display=swap" rel="stylesheet">
  <meta name="theme-color" content="#fafafa">
</head>
<body>
  <!--[if IE]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->
  <!-- Add your site or application content here -->

    <!--Navbar-->
    <?php  include 'includes/menu.php'; ?>


  <!--Hero-->
<div class="hero-image">
    <div class="hero-text f-jomalhari">
      <h1 class="h1"><?php echo $enterprise["textprincipale"];?></h1>
      <!-- Hover #2 -->

      <a href="boutique.php">
          <div class="box-2 mt-5">
            <div class="btn btn-two">
              <p class="mt-3 f-abel">Acheter</p>
            </div>
          </div>
        </a>
    </div>
  </div>

<div class="container mt-5 wow animated fadeInUp " data-wow-duration="0.4s">
  <div class="row mt-5">
    <div class="col-12 text-center my-5 lema">
      <p class="h2 f-jomalhari"><?php echo $enterprise["embleme"];?></p>
      <p><span class="h5 f-jomalhari wow animated fadeInUp" data-wow-duration="0.4s"><?php echo $enterprise["subembleme"];?></span></p>
    </div>
  </div>
</div>

<!--CATEGORIES-->

<section>
  <div class="cotenido-recientes display-block text-center">

  <a href="boutique.php?type=chocolat#produits"><div class="img-cat categoria-chocolate">
    <p class="text-cat">Chocolaterie</p>
    </div></a>

    <a href="boutique.php?type=patisserie#produits"><div class="img-cat categoria-patisserie">
      <p class="text-cat">Pâtisserie</p>
    </div></a>

    <a href="boutique.php?type=glace#produits"><div class="img-cat categoria-glass">
    <p class="text-cat">Glacerie</p>
    </div></a>

    <a href="boutique.php?type=gourmandise#produits"><div class="img-cat categoria-tablettes">
        <p class="text-cat">Gourmandise</p>
      </div></a>

</div>
</section>


<!--NUEVOS CHOCOLATES-->
<div class="container nuevos-chocolates">
  <div class="row text-center">
    <div class="col-12 text-center">
      <h3 class="h1 mt-5 mb-0 title text-center wow animated fadeInUp " data-wow-duration="0.5s">Les nouveautes de la saison</h3>
      <hr class="ml-auto mr-auto separador">
      <p class="f-roboto my-4 descripcion text-center wow animated fadeInUp " data-wow-duration="0.5s"><?php echo $enterprise["textsaison"];?>
      </p>
    </div>
  </div>


  <button id="id01" style="background-color:white !important; border:0px;" data-toggle="modal" data-target="#myModal"></button>

  <div class="row wow animated fadeInUp " data-wow-duration="0.5s">
      <div class="owl-carousel owl-theme">


      <?php
            $sql = "SELECT * FROM chocolats WHERE saison= 1";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    ?>
                    <form action="includes/panier.php" method="POST">
                      <a href="" class="vinc-produit" >
                        <div class="col-md-4 mt-5 ml-5 ml-md-2" id="id01"  data-toggle="modal" data-target="#myModal">
                          <div class="item text-center m-2">
                              <div class="card card-saison" style="width: 17rem;">
                                <img src="<?php echo $row["image_chocolat"]; ?>" height="200px"  style="object-fit: cover;" class="card-img-top card-img-top producto-imagen gallery-image " alt="...">
                                <div class="card-body">
                                  <h5 class="card-title"><?php echo $row["nom"]; ?></h5>
                                  <p class="card-text"><?php echo $row["prix"] , "€"; ?></p>
                                  </a>
                                      <input type="hidden" name="nom" value="<?php echo $row["nom"]; ?>">
                                      <input type="hidden" name="prix" value="<?php echo $row["prix"]; ?>">
                                      <input type="hidden" name="id" value="<?php echo $row["id"]; ?>">
                                  <button  name="index" class="btn btn-info w-25" data-toggle="modal" data-target="#modalpanier"><i class="fas fa-cart-plus"></i></button>
                                </div>
                              </div>
                            </div>
                        </div>
                 </form>
        <?php  }
            } else {
              ?>

            <?php
              }

            ?>
      </div>
  </div>
</div>

<!--Proximos eventos-->
<section>
  <div class="container pb-5 " id="Evenements">
    <div class="row">
      <div class="col">
          <h3 class="h1 mt-5 title text-center wow animated fadeInUp " data-wow-duration="0.5s">Prochains Événements</h3>
          <hr class="ml-auto mr-auto separador">

        </div>
    </div>
    <div class="row  wow animated fadeInUp text-center mt-4" data-wow-duration="0.5s">
        <?php
            $sql = "SELECT * FROM evenement";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    ?>

          <div class="col-md-4 mt-4 evento ml-auto mr-auto">
              <div class="card text-center">
                <div class="card card-i" >

                  <img  src="<?php echo $row["image_evenement"]; ?>" alt="Image d'evenement" height="200px"  style="object-fit: cover;">
                </div>
                <div class="card-body p-0">
                  <h5 class="bg-color-primary text-white block py-2 text-center"> <span class="mr-3"><?php echo date("d/m/Y", strtotime($row["date_evenement"])). " "; ?></span>   <span class="ml-3"> <?php echo $row["houre"]; ?></span> </h5>
                  <div class="contenido-evento px-3">
                  <h3 class="h3 text-left"> <?php echo $row["nom"]; ?> </h3>
                  <p class="text-left text-color-primary h5  my-3"> <?php echo $row["type_evenement"]; ?> </p>
                  <p class="text-left text-secondary">Prix: <?php echo $row["prix"], "€"; ?> </p>
                  <p class="text-left my-3 contenedor-description"><?php echo $row["description_evenement"]; ?> </p>
                   <?php
                  if (  $row["places"] >= 0){
                    ?>
                     <p class="text-left text-secondary">Places disponibles :
                    <?php
                    echo  $row["places"];
                    }else{
                      ?>
                      <hr class="mb-3 pb-2">
                      <span style="color: rgb(5, 139, 140);" class="">
                      <?php
                      echo  "Plus de places disponibles";
                      ?>
                        </span>
                      <?php
                    }
                  ?> </p>

                </div>


              <?php
              if (  $row["places"] >= 0){
                ?>
               <hr>

              <button type="button mb" class="btn btn-outline-success btn" data-toggle="modal" data-target="#exampleModal<?php echo $row["id"]; ?>">
                  S'inscrire
                </button>

                <?php
              }
                ?>
                </div>
                <div class="card-footer text-muted mt-3">
                  <div class="row">
                    <div class="col">
                      <a href="mailto:test@test.com"><i class="fas fa-envelope"></i></a>
                    </div>
                    <div class="col">
                      <a href="tel:+123456789"><i class="fas fa-phone"></i></a>
                    </div>
                  </div>
                </div>
              </div>
          </div>

         <?php }
            } else { ?>
              <h3 class="h2 f-jomalhari text-center text-secondary ml-auto mr-auto mt-5 pt-5"> <?php echo "Evénements à venir"; ?></h3>
            <?php
              }
            ?>

        </div>
      </div>
    </div>
  </div>
</section>

<?php
    $sql = "SELECT * FROM evenement";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
          ?>
            <!-- Modal -->
            <div class="modal fade" id="exampleModal<?php echo $row["id"]; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">s'inscrir à <?php echo $row["nom"]; ?></h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                    <form action="db/insert_inscriptor.php" method="POST">

                    <div class="form-group">
                      <label for="nom">Nom et Prenom</label>
                      <input type="text" name="nom" class="form-control" id="nom" placeholder="" required>
                    </div>

                    <input type="hidden" name="id_evenement" id="id_evenement" value="<?php echo $row["id"]; ?>">
                    <input type="hidden" name="places" value="<?php echo $row["places"]; ?>">

                    <div class="form-group">
                      <label for="email">Email address</label>
                      <input type="email" name="email" class="form-control" id="email" placeholder="name@example.com" required>
                    </div>

                    <div class="form-group">
                      <label for="telephone">Numero telephone</label>
                      <input type="tel" name="telephone" class="form-control" id="telephone" placeholder="07 12 34 .." required>
                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Fermer</button>
                      <input type="submit" id="submit" class="btn btn-outline-success" value="s'inscrir">

                    </div>
                    </form>

                  </div>
                </div>
              </div>
              </div>
          <?php
              }
          }
          ?>

<h3 class="h1 title text-center wow animated fadeInUp " data-wow-duration="0.5s">La galerie</h3>
<hr class="ml-auto mr-auto separador">

    <!--GALERY-->
<div class="container-fluid  wow animated fadeInUp " data-wow-duration="0.5s">
    <div class="container mb-5">
        <div class="row gallery">
            <figure class="col-md-4 m-0 p-0">
              <a href="#" data-toggle="modal" data-target="#imagen1">
                <img alt="picture" src="<?php echo $enterprise["img_galerie_1"];?>"
                    class="img-fluid gallery-image" />
                  </a>
                </figure>

                <figure class="col-md-4 m-0 p-0">
                  <a href="#" data-toggle="modal" data-target="#imagen2">
                    <img alt="picture" src="<?php echo $enterprise["img_galerie_2"];?>"
                    class="img-fluid gallery-image" />
                  </a>
                </figure>

                <figure class="col-md-4 m-0 p-0">
                <a href="#" data-toggle="modal" data-target="#imagen3">
                    <img alt="picture" src="<?php echo $enterprise["img_galerie_3"];?>"
                    class="img-fluid gallery-image" />
                  </a>
                </figure>

                <figure class="col-md-4 m-0 p-0">
                <a href="#" data-toggle="modal" data-target="#imagen4">
                    <img alt="picture" src="<?php echo $enterprise["img_galerie_4"];?>"
                    class="img-fluid gallery-image" />
                  </a>
                </figure>

                <figure class="col-md-4 m-0 p-0">
                <a href="#" data-toggle="modal" data-target="#imagen5">
                    <img alt="picture" src="<?php echo $enterprise["img_galerie_5"];?>"
                    class="img-fluid gallery-image" />
                  </a>
                </figure>

                <figure class="col-md-4 m-0 p-0">
                <a href="#" data-toggle="modal" data-target="#imagen6">
                    <img alt="picture" src="<?php echo $enterprise["img_galerie_6"];?>"
                    class="img-fluid gallery-image" />
                  </a>
                </figure>

                <figure class="col-md-4 m-0 p-0">
                  <a href="#" data-toggle="modal" data-target="#imagen7">
                    <img alt="picture" src="<?php echo $enterprise["img_galerie_7"];?>"
                    class="img-fluid gallery-image" />
                  </a>

                </figure>


                <figure class="col-md-4 m-0 p-0">
                <a href="#" data-toggle="modal" data-target="#imagen8">
                    <img alt="picture" src="<?php echo $enterprise["img_galerie_8"];?>"
                    class="img-fluid gallery-image" />
                  </a>
                </figure>

                <figure class="col-md-4 m-0 p-0">
                <a href="#" data-toggle="modal" data-target="#imagen9">
                    <img alt="picture" src="<?php echo $enterprise["img_galerie_9"];?>"
                      class="img-fluid gallery-image" />
                  </a>

                </figure>

              </div>

            </div>
          </div>
     </div>
  </div>


<?php if(isset($_GET["Ajoute"])){ ?>
  <script type="text/javascript">
    function redireccionar(){
    document.getElementById('id01').style.display='active';
    $("#id01").trigger("click");
  }
  setTimeout ("redireccionar()", 1000); //tiempo expresado en milisegundos
</script>
<?php  } ?>



<div id="myModal" class="modal fade" role="dialog">
  <div  class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Notification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      ajouté au panier avec succès
      </div>
      <div class="modal-footer">
        <button type="button" id="id01" class="btn btn-outline-secondary" data-dismiss="modal">Fermer</button>
        <a class="btn btn-outline-primary" href="panier.php">Voir panier</a>

      </div>
    </div>
  </div>
</div>



 <!--LES MODALS-->
<div id="imagen1" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
          <img src="<?php echo $enterprise["img_galerie_1"];?>" class="img-fluid">
      </div>
  </div>
</div>

<div id="imagen2" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
          <img src="<?php echo $enterprise["img_galerie_2"];?>" class="img-fluid">
      </div>
  </div>
</div>

<div id="imagen3" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
          <img src="<?php echo $enterprise["img_galerie_3"];?>" class="img-fluid">
      </div>
  </div>
</div>

<div id="imagen4" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
          <img src="<?php echo $enterprise["img_galerie_4"];?>" class="img-fluid">
      </div>
  </div>
</div>

<div id="imagen5" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
          <img src="<?php echo $enterprise["img_galerie_5"];?>" class="img-fluid">
      </div>
  </div>
</div>

<div id="imagen6" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
          <img src="<?php echo $enterprise["img_galerie_6"];?>" class="img-fluid">
      </div>
  </div>
</div>

<div id="imagen7" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
          <img src="<?php echo $enterprise["img_galerie_7"];?>" class="img-fluid">
      </div>
  </div>
</div>

<div id="imagen8" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
          <img src="<?php echo $enterprise["img_galerie_8"];?>" class="img-fluid">
      </div>
  </div>
</div>

<div id="imagen9" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
          <img src="<?php echo $enterprise["img_galerie_9"];?>" class="img-fluid">
      </div>
  </div>
</div>

<!--ABOUT US-->
<section class=" pb-0" id="apropos">
  <div class="container-fluid img-about my-0 py-0">
    <div class="container wow animated fadeInUp text-center" data-wow-duration="0.5s">
      <h3 class="h1 py-5 title text-center">À propos de nous</h3>
      <p class="pb-5 f-roboto text-center"><?php echo $enterprise["Apropos"];?></p>
    </div>
  </div>
</section>

<!--MAPA-->
<section class="py-0">
  <div class="container-fluid p-0 m-0">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2618.134784594804!2d2.321106851119591!3d48.98899239412031!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e6684569d8a4b9%3A0x60876b2b55393ed0!2s15%20Rue%20Saint-Jacques%2C%2095160%20Montmorency!5e0!3m2!1ses!2sfr!4v1573988705625!5m2!1ses!2sfr" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
  </div>
  </section>




  <!--FOOTER-->

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!-- Footer -->
	<section id="footer">
		<div class="container">
			<div class="row text-center text-xs-center text-sm-left text-md-left">
				<div class="col-xs-12 col-sm-4 col-md-4">
					<h5>Navegation</h5>
					<ul class="list-unstyled quick-links">
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Home</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Events</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>About</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Boutique</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					<h5>Boutique</h5>
					<ul class="list-unstyled quick-links">
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Chocolaterie</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Patisserie</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Glace</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Gourmandise</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					<h5>Informations</h5>
					<ul class="list-unstyled quick-links">
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Home</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>About</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>FAQ</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Get Started</a></li>
						<li><a href="https://wwwe.sunlimetech.com" title="Design and developed by"><i class="fa fa-angle-double-right"></i>Imprint</a></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
					<ul class="list-unstyled list-inline social text-center">
						<li class="list-inline-item"><a href="javascript:void();"><i class="fab fa-facebook-square"></i></a></li>
						<li class="list-inline-item"><a href="javascript:void();"><i class="fab fa-instagram"></i></a></li>
            <li class="list-inline-item"><a href="javascript:void();"><i class="fab fa-whatsapp"></i></a></li>

						<li class="list-inline-item"><a href="javascript:void();" target="_blank"><i class="fa fa-envelope"></i></a></li>
					</ul>
				</div>
				</hr>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
					<p class="h6">&copy All right Reversed.<a class="text-green ml-2" href="https://www.sunlimetech.com" target="_blank">Chocolaterie</a></p>
				</div>
				</hr>
			</div>
		</div>
	</section>
	<!-- ./Footer -->



  <!--SCRIPT-->

  <script src="js/vendor/modernizr-3.7.1.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
  <script src="js/plugins.js"></script>
  <script src="js/main.js"></script>
  <script src="js/wow.min.js"></script>
  <script>

    wow = new WOW(
        {
        boxClass:     'wow',      // default
        animateClass: 'animated', // default
        offset:       30,          // default
        mobile:       true,       // default
        live:         true        // default
      }
      )
      wow.init();
    </script>


  <!--Boostrap js-->
  <script src="js/bootstrap.min.js"></script>
  <!--OWL CAROUSEL -->
  <script src="js/owl.carousel.js"></script>
  <script>
  $('.owl-carousel').owlCarousel({
    center: true,
    loop:true,
    margin:6,
    nav:true,
    autoplay:true,
    autoplayTimeout:2500,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:4
        }
    }
});
  </script>


  <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
  <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('set','transport','beacon'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async></script>
</body>

</html>
