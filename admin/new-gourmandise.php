<?php
session_start();
if ($_SESSION['user'] == null ||  $_SESSION['user'] == ''){
	header("Location: index.php");
	}else{
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Nuevo item</title>

    <!-- Normalize V8.0.1 -->
    <link rel="stylesheet" href="./css/normalize.css">

    <!-- Bootstrap V4.3 -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">

    <!-- Bootstrap Material Design V4.0 -->
    <link rel="stylesheet" href="./css/bootstrap-material-design.min.css">

    <!-- Font Awesome V5.9.0 -->
    <link rel="stylesheet" href="./css/all.css">

    <!-- Sweet Alerts V8.13.0 CSS file -->
    <link rel="stylesheet" href="./css/sweetalert2.min.css">

    <!-- Sweet Alert V8.13.0 JS file-->
    <script src="./js/sweetalert2.min.js"></script>

    <!-- jQuery Custom Content Scroller V3.1.5 -->
    <link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">

    <!-- General Styles -->
    <link rel="stylesheet" href="./css/style.css">


</head>


<body>
    <!-- Main container -->
    <main class="full-box main-container">
        	<!--Menu-->
	<?php
	include 'includes/menu.php';
	?>

        <section class="full-box page-content">
            <nav class="full-box navbar-info">
                <a href="#" class="float-left show-nav-lateral">
                    <i class="fas fa-exchange-alt"></i>
                </a>
                <a href="user-update.html">
                    <i class="fas fa-user-cog"></i>
                </a>
                <a href="#" class="btn-exit-system">
                    <i class="fas fa-power-off"></i>
                </a>
            </nav>
            <!-- Page header -->
            <div class="full-box page-header">
                <h3 class="text-left">
                    <i class="fas fa-plus fa-fw"></i> &nbsp; Ajouter une 	Gourmandise</h3>
                <p class="text-justify">
				Ici, vous pouvez ajouter un nouveau produit </p>
            </div>
            <div class="container-fluid">
                <ul class="full-box list-unstyled page-nav-tabs">
					<li>
                        <a class="active" href="new-chocolat"><i class="fas fa-plus fa-fw"></i> &nbsp; ADD ITEMS</a>
                    </li>
                    <li>
                        <a href="produit-list.php"><i class="fas fa-clipboard-list fa-fw"></i> &nbsp; LISTE DES PRODUITS</a>
                    </li>
                </ul>
            </div>

            <!--CONTENT-->
            <div class="container-fluid">
				<form action="../db/insert_chocolat.php" method="POST" class="form-neon" autocomplete="off">
					<fieldset>
						<legend><i class="far fa-plus-square"></i> &nbsp; Information du produit</legend>
						<div class="container-fluid">
							<div class="row">
								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="nom_produit" class="bmd-label-floating">Nom du produit (70)</label>
										<input type="text" class="form-control" name="nom_produit" id="nom_produit" maxlength="70" required>
									</div>
								</div>

								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="produit_prix" class="bmd-label-floating">Prix</label>
										<input type="text" pattern="[0-9.,]{1,20}" class="form-control" name="produit_prix" id="produit_prix" required>
									</div>
								</div>

								<div class="col-12 col-md-12">
									<div class="form-group">
										<label for="image" class="bmd-label-floating">image</label>
										<input type="text" class="form-control" name="image" id="image" required>									</div>
								</div>


								<div class="col-12 col-md-12">
									<div class="form-group">
										<label for="produit-contenu" class="bmd-label-floating">Description</label>
										<textarea class="form-control" name="produit-contenu" id="produit-contenu" rows="10" required></textarea>
										<input type="checkbox" class="form-check-input mt-5" name="saison" value="1" id="exampleCheck1">
   										 <label class="form-check-label mt-5"  for="exampleCheck1">Gourmandise de la saison</label>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" name="type" value="gourmandise">
					</fieldset>
					<br><br><br>
					<p class="text-center" style="margin-top: 40px;">
						<button type="reset" class="btn btn-raised btn-secondary btn-sm"><i class="fas fa-paint-roller"></i> &nbsp; Effacer</button>
						&nbsp; &nbsp;
						<button type="submit" class="btn btn-raised btn-info btn-sm"><i class="far fa-save"></i> &nbsp; Sauvegarder</button>
					</p>
				</form>
			</div>
        </section>




    </main>


	<!--=============================================
	=            Include JavaScript files           =
	==============================================-->
	<!-- jQuery V3.4.1 -->
	<script src="./js/jquery-3.4.1.min.js" ></script>

	<!-- popper -->
	<script src="./js/popper.min.js" ></script>

	<!-- Bootstrap V4.3 -->
	<script src="./js/bootstrap.min.js" ></script>

	<!-- jQuery Custom Content Scroller V3.1.5 -->
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>

	<!-- Bootstrap Material Design V4.0 -->
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>

	<script src="./js/main.js" ></script>
</body>
</html>