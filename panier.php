<?php
include 'db/conn.php';
session_start();
?>

<!doctype html>
<html class="no-js" lang="fr">

<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->
  <!--OWL Carrousel-->
  <link rel="stylesheet" href="css/owl.carousel.css">
  <link rel="stylesheet" href="css/owl.theme.default.css">
  <!--Boostrap-->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/main.css">
  <!--animated CSS-->
  <link rel="stylesheet" href="css/animate.css">
  <!--font Awesome-->
  <script src="https://kit.fontawesome.com/5fe0cb84c4.js" crossorigin="anonymous"></script>
  <!--Google font-->
  <link href="https://fonts.googleapis.com/css?family=Abel|Jomolhari|Roboto&display=swap" rel="stylesheet">
  <meta name="theme-color" content="#fafafa">

  
<style>
        /* Media query for mobile viewport */
        @media screen and (max-width: 400px) {
            #paypal-button-container {
                width: 100%;
            }
        }
        
        /* Media query for desktop viewport */
        @media screen and (min-width: 400px) {
            #paypal-button-container {
                width: 250px;
            }
        }
</style>

</head>
<body>
  <!--[if IE]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->
  <!-- Add your site or application content here -->

<!--Navbar-->
<?php  include 'includes/menu.php'; ?>


<!--panier-->
<div class="container  mt-5">
<div class="row text-center">
    <div class="col-12 text-center">
      <h3 class="h1 mt-5 mb-0 title text-center wow animated fadeInUp " data-wow-duration="0.5s">panier</h3>
      <hr class="ml-auto mr-auto separador">
    </div>
</div>
<div class="row">



    <?php $total= 0; $contador = 0;?>
    <?php if(!empty($_SESSION['CARRITO'])) {  ?>

    <table class="table table-light table-bordered text-center">
      <tbody>
        <tr>
          <th width=40% class="text-center">Nombre</th>
          <th width=20% class="text-center">precio</th>
          <th width=5%>-</th>
        </tr>
        <?php foreach($_SESSION['CARRITO'] as $indice=>$producto) { ?>
        <tr>
          <td width=40% class="text-center"> <?php echo $producto['NOMBRE']; ?> </td>
          <td width=20% class="text-center"> <?php echo number_format($producto['PRIX'],2), "€";?></td>
          <form action="includes/panier.php" method="POST">
          <input type="hidden" name="id" value="<?php echo $indice ?>">
          <td width=5% class="text-center"><button type="input" name="eliminar" class="btn btn-outline-danger text-center">x</button></td>
          </form>
        </tr>
        <?php $total= $producto['PRIX'] + $total;  $contador = $contador + 1; ?>
        <?php } ?>
        <tr>
          <td  colspan="1" align="right"><h4>Total</h4></td>
          <td class="text-center" align="right"><h4><?php echo number_format($total,2),"€"; ?></h4></td>
          <td> <button class="btn btn-outline-success"  data-toggle="modal" data-target="#exampleModal">Continuer</button> </td>

        </tr>
      </tbody>
    </table>

<?php }else { ?>
  <div class="col-12 text-center">
  <h3 class="h2 f-jomalhari text-center text-secondary ml-auto mr-auto mt-5 pt-5"> <?php echo "Votre panier est vide pour le moment."; ?></h3>
    </div>

  <?php } ?>

    
  </div>
</div>

<button id="id01" style="background-color:white !important; border:0px;" data-toggle="modal" data-target="#myModal"></button>

<!--NUEVOS CHOCOLATES-->
<div class="container nuevos-chocolates mt-5">
  <div class="row text-center">
    <div class="col-12 text-center">
      <h3 class="h1 mt-5 mb-0 title text-center wow animated fadeInUp " data-wow-duration="0.5s">Les nouveautes de la saison</h3>
      <hr class="ml-auto mr-auto separador">

    </div>
  </div>



  <div class="row wow animated fadeInUp " data-wow-duration="0.5s">
      <div class="owl-carousel owl-theme">
        
        
      <?php

$sql = "SELECT * FROM chocolats WHERE saison= 1";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        ?>
        <form action="includes/panier.php" method="POST">
          <a href="" class="vinc-produit">
            <div class="col-md-4 mt-5 ml-5 ml-md-2">
              <div class="item text-center m-2">
                  <div class="card card-saison" style="width: 17rem;">
                    <img src="<?php echo $row["image_chocolat"]; ?>" height="200px"  style="object-fit: cover;" class="card-img-top card-img-top producto-imagen gallery-image " alt="...">
                    <div class="card-body">
                      <h5 class="card-title"><?php echo $row["nom"]; ?></h5>
                      <p class="card-text">$<?php echo $row["prix"]; ?></p>
                      </a>
                          <input type="hidden" name="nom" value="<?php echo $row["nom"]; ?>">
                          <input type="hidden" name="prix" value="<?php echo $row["prix"]; ?>">
                          <input type="hidden" name="id" value="<?php echo $row["id"]; ?>">
                      <button type="submit" name="panier" class="btn btn-info w-25" data-toggle="modal" data-target="#modalpanier"><i class="fas fa-cart-plus"></i></button>
                    </div>
                  </div>
                </div>
            </div>
     </form>
<?php  }
} else {
  ?>

<?php
  }

?>
</div>
</div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">méthode de paiement</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      <div class="container-fluid">


        <div class="text-center">
        <h5>  <?php echo $contador , " produits - "; ?> <?php echo number_format($total,2),"€" ?></h5>
      <h4 class=""></h4>
        </div>
      </div>          
      </div>
      <div class="modal-footer mr-auto ml-auto"> 
      <div id="paypal-button"></div>
      </div>
    </div>
  </div>
</div>


<div id="myModal" class="modal fade" role="dialog">
  <div  class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Notification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      ajouté au panier avec succès
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Voir panier</button>
        
        
      </div>
    </div>
  </div>
</div>
  

<!--ABOUT US-->
<section class=" pb-0" id="apropos">
  <div class="container-fluid img-about my-0 py-0">
    <div class="container wow animated fadeInUp " data-wow-duration="0.5s">
      <h3 class="h1 py-5 title text-center">À propos de nous</h3>
      <p class="pb-5 f-roboto">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus provident eaque repudiandae saepe quas soluta consequatur. Inventore dolorum voluptate dolores neque qui quidem ex dicta nulla iure, doloribus pariatur. Deserunt?
       caecati asperiores officia praesentium accusantium aut maiores corrupti nemo explicabo accusamus veniam architecto ipsa ipsam omnis rerum et modi nobis rem. Natus, esse?</p>
    </div>
  </div>
</section>

<!--MAPA-->
<section class="py-0">
  <div class="container-fluid p-0 m-0">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2618.134784594804!2d2.321106851119591!3d48.98899239412031!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e6684569d8a4b9%3A0x60876b2b55393ed0!2s15%20Rue%20Saint-Jacques%2C%2095160%20Montmorency!5e0!3m2!1ses!2sfr!4v1573988705625!5m2!1ses!2sfr" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
  </div>
  </section>


  

  <!--FOOTER-->

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!-- Footer -->
	<section id="footer">
		<div class="container">
			<div class="row text-center text-xs-center text-sm-left text-md-left">
				<div class="col-xs-12 col-sm-4 col-md-4">
					<h5>Navegation</h5>
					<ul class="list-unstyled quick-links">
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Home</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Events</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>About</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Boutique</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					<h5>Boutique</h5>
					<ul class="list-unstyled quick-links">
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Chocolaterie</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Patisserie</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Glace</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Gourmandise</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					<h5>Informations</h5>
					<ul class="list-unstyled quick-links">
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Home</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>About</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>FAQ</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Get Started</a></li>
						<li><a href="https://wwwe.sunlimetech.com" title="Design and developed by"><i class="fa fa-angle-double-right"></i>Imprint</a></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
					<ul class="list-unstyled list-inline social text-center">
						<li class="list-inline-item"><a href="javascript:void();"><i class="fab fa-facebook-square"></i></a></li>
						<li class="list-inline-item"><a href="javascript:void();"><i class="fab fa-instagram"></i></a></li>
            <li class="list-inline-item"><a href="javascript:void();"><i class="fab fa-whatsapp"></i></a></li>

						<li class="list-inline-item"><a href="javascript:void();" target="_blank"><i class="fa fa-envelope"></i></a></li>
					</ul>
				</div>
				</>
			</div>	
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
					<p class="h6">&copy All right Reversed.<a class="text-green ml-2" href="https://www.sunlimetech.com" target="_blank">Chocolaterie</a></p>
				</div>
				</hr>
			</div>	
		</div>
	</section>
  <!-- ./Footer -->

<?php if(isset($_GET["Ajoute"])){ ?>
  <script type="text/javascript">
    function redireccionar(){
    document.getElementById('id01').style.display='active';
    $("#id01").trigger("click");
  } 
  setTimeout ("redireccionar()", 1000); //tiempo expresado en milisegundos
  </script>
<?php  } ?>

<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script>
  paypal.Button.render({
    // Configure environment
    env: 'sandbox',
    client: {
      sandbox: 'AWMQhCrqA0wy2gOOzxg9s5Z-GwB682illVszIkuERcLlPmsK8v1871dDpcQRw5KNzErMENDNmZrXyQZx',
      production: 'ATC2sl2xze-LZblD43j03EesGI25KpPT9vfR9sht5zMX5KLMLOvuWqQ_mY2LpHroIRRYSmQtSCA82RHh'
    },
    // Customize button (optional)
    locale: 'fr_FR',
    style: {
      size: 'small',
      color: 'blue',
      shape: 'pill',
    },

    // Enable Pay Now checkout flow (optional)
    commit: true,

    // Set up a payment
    payment: function(data, actions) {
      return actions.payment.create({
        transactions: [{
          amount: {
            total: '<?php echo number_format($total,2) ?>',
            currency: 'EUR',
          }
        }]
      });
    },
    // Execute the payment
    onAuthorize: function(data, actions) {
      return actions.payment.execute().then(function() {
        // Show a confirmation message to the buyer
        window.alert('Payment Realicé');
      });
    }
  }, '#paypal-button');

</script>


  <!--SCRIPT-->
  <script src="js/vendor/modernizr-3.7.1.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
  <script src="js/plugins.js"></script>
  <script src="js/main.js"></script>
  <script src="js/wow.min.js"></script>
  <script>

    wow = new WOW(
        {
        boxClass:     'wow',      // default
        animateClass: 'animated', // default
        offset:       30,          // default
        mobile:       true,       // default
        live:         true        // default
      }
      )
      wow.init();
    </script>


  <!--Boostrap js-->
  <script src="js/bootstrap.min.js"></script>
  <!--OWL CAROUSEL -->
  <script src="js/owl.carousel.js"></script>
  <script>
  $('.owl-carousel').owlCarousel({
    center: true,
    loop:true,
    margin:6,
    nav:true,
    autoplay:true,
    autoplayTimeout:2500,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:4
        }
    }
});
  </script>


</html>
