<?php
include 'db/conn.php';
session_start();
?>

<!doctype html>
<html class="no-js" lang="fr">

<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->
  <!--OWL Carrousel-->
  <link rel="stylesheet" href="css/owl.carousel.css">
  <link rel="stylesheet" href="css/owl.theme.default.css">

  <!--Boostrap-->
  <link rel="stylesheet" href="css/bootstrap.min.css">

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/main.css">

  <!--animated CSS-->
  <link rel="stylesheet" href="css/animate.css">

  <!--font Awesome-->
  <script src="https://kit.fontawesome.com/5fe0cb84c4.js" crossorigin="anonymous"></script>
  <!--Google font-->

  <link href="https://fonts.googleapis.com/css?family=Abel|Jomolhari|Roboto&display=swap" rel="stylesheet">


  <meta name="theme-color" content="#fafafa">


</head>

<body>

  <!--Navbar-->

  <?php  include 'includes/menu.php'; ?>
<!--NUEVOS CHOCOLATES-->
<div class="container nuevos-chocolates mt-5">
        <div class="row text-center">
          <div class="col-12 text-center">
            <h3 class="h1 mt-5 mb-0 title text-center wow animated fadeInUp " data-wow-duration="0.5s">Les nouveautes de la saison</h3>
            <hr class="ml-auto mr-auto separador">
          </div>
        </div>
        <div class="row wow animated fadeInUp " data-wow-duration="0.5s">
      <div class="owl-carousel owl-theme">
      <?php

$sql = "SELECT * FROM chocolats WHERE saison= 1";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        ?>
        <form action="includes/panier.php" method="POST">
          <a href="" class="vinc-produit">
            <div class="col-md-4 mt-5 ml-5 ml-md-2">
              <div class="item text-center m-2">
                  <div class="card card-saison" style="width: 17rem;">
                    <img src="<?php echo $row["image_chocolat"]; ?>" height="200px"  style="object-fit: cover;" class="card-img-top card-img-top producto-imagen gallery-image " alt="...">
                    <div class="card-body">
                      <h5 class="card-title"><?php echo $row["nom"]; ?></h5>
                      <p class="card-text"><?php echo $row["prix"], "€"; ?></p>
                      </a>
                          <input type="hidden" name="nom" value="<?php echo $row["nom"]; ?>">
                          <input type="hidden" name="prix" value="<?php echo number_format($row["prix"]); ?>">
                          <input type="hidden" name="id" value="<?php echo $row["id"]; ?>">
                      <button type="submit" name="boutique" class="btn btn-info w-25" data-toggle="modal" data-target="#modalpanier"><i class="fas fa-cart-plus"></i></button>
                    </div>
                  </div>
                </div>
            </div>
     </form>
<?php  }
} else {
  ?>

<?php
  }

?>
</div>
</div>
</div>

  <!--CATALOGUE-->

  <div class="row text-center mt-5 pt-5 mx-0" id="produits">
          <div class="col-12 text-center">
            <h3 class="h1 mt-5 mb-0 title text-center wow animated fadeInUp " data-wow-duration="0.5s">CATALOGUE</h3>
            <hr class="ml-auto mr-auto separador">
          </div>
        </div>

  <!--Catalogo responsivo-->

  <div id="mySidepanel" class="sidepanel my-0" id="catalogue-slide">
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
      <a href="boutique.php#produits">Tout</a>
      <a href="boutique.php?type=chocolat#produits" >Chocolaterie</a>
      <div class="border-left ml-4">
        <a href="boutique.php?type=bonbons#produits" class="font-weight-light">Bonbons au Chocolat</a>
        <a href="boutique.php?type=tablettes#produits"class="font-weight-light">Tablettes</a>
        <a href="boutique.php?type=special#produits"class="font-weight-light">Jour Spécial</a>
      </div>
      <a href="boutique.php?type=patisserie#produits">Pâtisserie</a>
      <a href="boutique.php?type=glace#produits">Glacerie</a>
      <div class="border-left ml-4">
        <a href="boutique.php?type=entremats#produits" class="font-weight-light">Entremets Glacés</a>
        <a href="boutique.php?type=pots#produits"class="font-weight-light">Pots de glace</a>
      </div>
      <a href="boutique.php?type=gourmandise#produits">Gourmandise</a>
    </div>
    <div class="container text-center">
    <button class="openbtn btn btn-outline-success text-center" onclick="openNav()">&#9776; Filtrer par</button>
  </div>


  <section class="mt-0 mb-5" >
    <div class="container contenedor-catalogo ml-5" style="width: 20%; float: left; display: block;">
      <div class="row">
        <div class="slider-categorias ml-auto mr-auto">
          <h3 class="h3 f-roboto">Acheter</h3>
          <hr class="ml-auto mr-auto separador-pequeño">
          <ul>
            <li><a href="boutique.php#produits" class="font-weight-bolder">Tout</a></li>
            <li><a href="boutique.php?type=chocolat#produits" class="font-weight-bolder">Chocolaterie</a></li>
            <div class="border-left">
              <li><a href="boutique.php?type=bonbons#produits" class="ml-3">Bonbons au Chocolat</a></li>
              <li><a href="boutique.php?type=tablettes#produits" class="ml-3">Tablettes</a></li>
              <li><a href="boutique.php?type=special#produits" class="ml-3">Jour Spécial</a></li>
            </div>
            <li><a href="boutique.php?type=patisserie#produits" class="font-weight-bolder">Pâtisserie</a></li>
            <li><a href="boutique.php?type=glace#produits" class="font-weight-bolder">Glacerie</a></li>

          <div class="border-left">
            <li><a href="boutique.php?type=entremets#produits" class="ml-3">Entremets Glacés</a></li>
            <li><a href="boutique.php?type=pots#produits" class="ml-3">Pots de glace</a></li>
          </div>
            <li><a href="boutique.php?type=gourmandise#produits" class="font-weight-bolder">Gourmandise</a></li>
          </ul>
        </div>
      </div>
    </div>

    <div class="container-fluid ml-auto mr-auto"id="catalogue">
    <button id="id01" style="background-color:white !important; border:0px;" data-toggle="modal" data-target="#myModal"></button>

    <div class="row ml-auto  mr-auto mb-5">



  <?php
  if(isset($_GET['type'])){

      if($_GET['type'] == "chocolat"){
        //Chocolates
    $sql = "SELECT * FROM chocolats WHERE `type_chocolat` = 'chocolat'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            ?>
        <form action="includes/panier.php" method="POST" style="width: 300px;">
          <a href="" class="vinc-produit">
            <div class="col-md-4 mt-5 ml-5 ml-md-2">
              <div class="item text-center m-2">
              <div class="card card-saison" style="width: 15rem;">
                    <img src="<?php echo $row["image_chocolat"]; ?>" height="200px"  style="object-fit: cover;" class="card-img-top card-img-top producto-imagen gallery-image " alt="...">
                    <div class="card-body">
                      <h5 class="card-title"><?php echo $row["nom"]; ?></h5>
                      <p class="card-text"><?php echo number_format($row["prix"], 2), "€";; ?></p>
                      </a>
                          <input type="hidden" name="nom" value="<?php echo $row["nom"]; ?>">
                          <input type="hidden" name="prix" value="<?php echo number_format($row["prix"], 2); ?>">
                          <input type="hidden" name="id" value="<?php echo $row["id"]; ?>">
                      <button type="submit" name="boutique" class="btn btn-info w-25" data-toggle="modal" data-target="#modalpanier"><i class="fas fa-cart-plus"></i></button>
                    </div>
                  </div>
                </div>
            </div>
     </form>

            <?php
               }
            } else {
              ?>
              <h3 class="h2 f-jomalhari text-center text-secondary  ml-auto mr-auto mt-5 pt-5"> <?php echo "Chocolats à venir"; ?></h3>

            <?php
              }
            }
            //Patisserie
         if($_GET['type'] == "patisserie"){
              //Chocolates
          $sql = "SELECT * FROM chocolats WHERE `type_chocolat` = 'patisserie'";
          $result = $conn->query($sql);
          if ($result->num_rows > 0) {
              // output data of each row
              while($row = $result->fetch_assoc()) {
                  ?>
                <div action="includes/panier.php" method="POST" style="width: 300px;">
                  <a href="" class="vinc-produit">
                    <div class="col-md-4 mt-5 ml-5 ml-md-2">
                      <div class="item text-center m-2">
                      <div class="card card-saison" style="width: 15rem;">
                            <img src="<?php echo $row["image_chocolat"]; ?>" height="200px"  style="object-fit: cover;" class="card-img-top card-img-top producto-imagen gallery-image " alt="...">
                            <div class="card-body">
                              <h5 class="card-title"><?php echo $row["nom"]; ?></h5>
                              <p class="card-text"><?php echo number_format($row["prix"], 2), "€"; ?></p>
                              </a>
                                  <input type="hidden" name="nom" value="<?php echo $row["nom"]; ?>">
                                  <input type="hidden" name="prix" value="<?php echo number_format($row["prix"], 2); ?>">
                                  <input type="hidden" name="id" value="<?php echo $row["id"]; ?>">
                              <button type="submit" name="boutique" class="btn btn-info w-25" data-toggle="modal" data-target="#modalpanier"><i class="fas fa-cart-plus"></i></button>
                            </div>
                          </div>
                        </div>
                    </div>
                 </div>

                  <?php
                     }
                  } else {
                    ?>
              <h3 class="h2 f-jomalhari text-center text-secondary  ml-auto mr-auto mt-5 pt-5"> <?php echo "Patisserie à venir"; ?></h3>

                  <?php
                    }
                  }





      //Glace
         if($_GET['type'] == "glace"){
      //Chocolates
      $sql = "SELECT * FROM chocolats WHERE `type_chocolat` = 'glace'";
      $result = $conn->query($sql);
      if ($result->num_rows > 0) {
          // output data of each row
          while($row = $result->fetch_assoc()) {
              ?>
        <form action="includes/panier.php" method="POST">
          <a href="" class="vinc-produit">
            <div class="col-md-4 mt-5 ml-5 ml-md-2">
              <div class="item text-center m-2">
              <div class="card card-saison" style="width: 15rem;">
                    <img src="<?php echo $row["image_chocolat"]; ?>" height="200px"  style="object-fit: cover;" class="card-img-top card-img-top producto-imagen gallery-image " alt="...">
                    <div class="card-body">
                      <h5 class="card-title"><?php echo $row["nom"]; ?></h5>
                      <p class="card-text"><?php echo number_format($row["prix"], 2), "€"; ?></p>
                      </a>
                          <input type="hidden" name="nom" value="<?php echo $row["nom"]; ?>">
                          <input type="hidden" name="prix" value="<?php echo number_format($row["prix"], 2); ?>">
                          <input type="hidden" name="id" value="<?php echo $row["id"]; ?>">
                      <button type="submit" name="boutique" class="btn btn-info w-25" data-toggle="modal" data-target="#modalpanier"><i class="fas fa-cart-plus"></i></button>
                    </div>
                  </div>
                </div>
            </div>
      </form>

        <?php
            }
        } else {
          ?>
        <h3 class="h2 f-jomalhari text-center text-secondary  ml-auto mr-auto mt-5 pt-5 mb-5"> <?php echo "glace à venir"; ?></h3>
          <div class="mb-5">
          .
          </div>
        <?php
          }
        }

          //Patisserie
         if($_GET['type'] == "gourmandise"){
          //Chocolates
      $sql = "SELECT * FROM chocolats WHERE `type_chocolat` = 'gourmandise'";
      $result = $conn->query($sql);
      if ($result->num_rows > 0) {
          // output data of each row
          while($row = $result->fetch_assoc()) {
              ?>
        <form action="includes/panier.php" method="POST">
          <a href="" class="vinc-produit">
            <div class="col-md-4 mt-5 ml-5 ml-md-2">
              <div class="item text-center m-2">
              <div class="card card-saison" style="width: 15rem;">
                    <img src="<?php echo $row["image_chocolat"]; ?>" height="200px"  style="object-fit: cover;" class="card-img-top card-img-top producto-imagen gallery-image " alt="...">
                    <div class="card-body">
                      <h5 class="card-title"><?php echo $row["nom"]; ?></h5>
                      <p class="card-text"><?php echo number_format($row["prix"], 2), "€"; ?></p>
                      </a>
                          <input type="hidden" name="nom" value="<?php echo $row["nom"]; ?>">
                          <input type="hidden" name="prix" value="<?php number_format($row["prix"], 2); ?>">
                          <input type="hidden" name="id" value="<?php echo $row["id"]; ?>">
                      <button type="submit" name="boutique" class="btn btn-info w-25" data-toggle="modal" data-target="#modalpanier"><i class="fas fa-cart-plus"></i></button>
                    </div>
                  </div>
                </div>
            </div>
     </form>
              <?php
                 }
              } else {
                ?>
              <h3 class="h2 f-jomalhari text-center text-secondary  ml-auto mr-auto mt-5 pt-5"> <?php echo "Gourmandise à venir"; ?></h3>

              <?php
                }
              }
    if($_GET['type'] == "bonbons"){
      //Chocolates
      $sql = "SELECT * FROM chocolats WHERE `sub_categorie` = 'Bonbons au chocolat'";
      $result = $conn->query($sql);
      if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
      ?>
          <form action="includes/panier.php" method="POST" style="width: 300px;">
            <a href="" class="vinc-produit">
              <div class="col-md-4 mt-5 ml-5 ml-md-2">
                <div class="item text-center m-2">
                    <div class="card card-saison" style="width: 15rem;">
                      <img src="<?php echo $row["image_chocolat"]; ?>" height="200px"  style="object-fit: cover;" class="card-img-top card-img-top producto-imagen gallery-image " alt="...">
                      <div class="card-body">
                        <h5 class="card-title"><?php echo $row["nom"]; ?></h5>
                        <p class="card-text"><?php echo number_format($row["prix"], 2), "€"; ?></p>
                        </a>
                            <input type="hidden" name="nom" value="<?php echo $row["nom"]; ?>">
                            <input type="hidden" name="prix" value="<?php echo number_format($row["prix"], 2); ?>">
                            <input type="hidden" name="id" value="<?php echo $row["id"]; ?>">
                        <button type="submit" name="boutique" class="btn btn-info w-25" data-toggle="modal" data-target="#modalpanier"><i class="fas fa-cart-plus"></i></button>
                      </div>
                    </div>
                  </div>
              </div>
       </form>
    <?php
      }
        } else {
          ?>
          <h3 class="h2 f-jomalhari text-center text-secondary ml-auto mr-auto mt-5 pt-5"> <?php echo "Chocolats à venir"; ?></h3>
          <?php
        }
      } ?>

<?php


    if($_GET['type'] == "tablettes"){
      //Chocolates
      $sql = "SELECT * FROM chocolats WHERE `sub_categorie` = 'Tablettes'";
      $result = $conn->query($sql);
      if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
      ?>
          <form action="includes/panier.php" method="POST" style="width: 300px;">
            <a href="" class="vinc-produit">
              <div class="col-md-4 mt-5 ml-5 ml-md-2">
                <div class="item text-center m-2">
                    <div class="card card-saison" style="width: 15rem;">
                      <img src="<?php echo $row["image_chocolat"]; ?>" height="200px"  style="object-fit: cover;" class="card-img-top card-img-top producto-imagen gallery-image " alt="...">
                      <div class="card-body">
                        <h5 class="card-title"><?php echo $row["nom"]; ?></h5>
                        <p class="card-text"><?php echo number_format($row["prix"], 2), "€"; ?></p>
                        </a>
                            <input type="hidden" name="nom" value="<?php echo $row["nom"]; ?>">
                            <input type="hidden" name="prix" value="<?php echo number_format($row["prix"], 2); ?>">
                            <input type="hidden" name="id" value="<?php echo $row["id"]; ?>">
                        <button type="submit" name="boutique" class="btn btn-info w-25" data-toggle="modal" data-target="#modalpanier"><i class="fas fa-cart-plus"></i></button>
                      </div>
                    </div>
                  </div>
              </div>
       </form>
    <?php
      }
        } else {
          ?>
          <h3 class="h2 f-jomalhari text-center text-secondary ml-auto mr-auto mt-5 pt-5"> <?php echo "Chocolats à venir"; ?></h3>
          <?php
        }
      }
?>

<?php
    if($_GET['type'] == "special"){
      //Chocolates
      $sql = "SELECT * FROM chocolats WHERE `sub_categorie` = 'special'";
      $result = $conn->query($sql);
      if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
      ?>
          <form action="includes/panier.php" method="POST" style="width: 300px;">
            <a href="" class="vinc-produit">
              <div class="col-md-4 mt-5 ml-5 ml-md-2">
                <div class="item text-center m-2">
                    <div class="card card-saison" style="width: 15rem;">
                      <img src="<?php echo $row["image_chocolat"]; ?>" height="200px"  style="object-fit: cover;" class="card-img-top card-img-top producto-imagen gallery-image " alt="...">
                      <div class="card-body">
                        <h5 class="card-title"><?php echo $row["nom"]; ?></h5>
                        <p class="card-text"><?php echo number_format($row["prix"], 2), "€"; ?></p>
                        </a>
                            <input type="hidden" name="nom" value="<?php echo $row["nom"]; ?>">
                            <input type="hidden" name="prix" value="<?php echo number_format($row["prix"], 2); ?>">
                            <input type="hidden" name="id" value="<?php echo $row["id"]; ?>">
                        <button type="submit" name="boutique" class="btn btn-info w-25" data-toggle="modal" data-target="#modalpanier"><i class="fas fa-cart-plus"></i></button>
                      </div>
                    </div>
                  </div>
              </div>
       </form>
    <?php
      }
        } else {
          ?>
          <h3 class="h2 f-jomalhari text-center text-secondary ml-auto mr-auto mt-5 pt-5"> <?php echo "Chocolats à venir"; ?></h3>
          <?php
        }
      }
?>


<?php
    if($_GET['type'] == "entremets"){
      //Chocolates
      $sql = "SELECT * FROM chocolats WHERE `sub_categorie` = 'Entremaint glace'";
      $result = $conn->query($sql);
      if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
      ?>
          <form action="includes/panier.php" method="POST" style="width: 300px;">
            <a href="" class="vinc-produit">
              <div class="col-md-4 mt-5 ml-5 ml-md-2">
                <div class="item text-center m-2">
                    <div class="card card-saison" style="width: 15rem;">
                      <img src="<?php echo $row["image_chocolat"]; ?>" height="200px"  style="object-fit: cover;" class="card-img-top card-img-top producto-imagen gallery-image " alt="...">
                      <div class="card-body">
                        <h5 class="card-title"><?php echo $row["nom"]; ?></h5>
                        <p class="card-text"><?php echo number_format($row["prix"], 2), "€"; ?></p>
                        </a>
                            <input type="hidden" name="nom" value="<?php echo $row["nom"]; ?>">
                            <input type="hidden" name="prix" value="<?php echo number_format($row["prix"], 2); ?>">
                            <input type="hidden" name="id" value="<?php echo $row["id"]; ?>">
                        <button type="submit" name="boutique" class="btn btn-info w-25" data-toggle="modal" data-target="#modalpanier"><i class="fas fa-cart-plus"></i></button>
                      </div>
                    </div>
                  </div>
              </div>
       </form>
    <?php
      }
        } else {
          ?>
          <h3 class="h2 f-jomalhari text-center text-secondary ml-auto mr-auto mt-5 pt-5"> <?php echo "Chocolats à venir"; ?></h3>
          <?php
        }
      }
?>

<?php
    if($_GET['type'] == "pots"){
      //Chocolates
      $sql = "SELECT * FROM chocolats WHERE `sub_categorie` = 'pots'";
      $result = $conn->query($sql);
      if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
      ?>
          <form action="includes/panier.php" method="POST" style="width: 300px;">
            <a href="" class="vinc-produit">
              <div class="col-md-4 mt-5 ml-5 ml-md-2">
                <div class="item text-center m-2">
                    <div class="card card-saison" style="width: 15rem;">
                      <img src="<?php echo $row["image_chocolat"]; ?>" height="200px"  style="object-fit: cover;" class="card-img-top card-img-top producto-imagen gallery-image " alt="...">
                      <div class="card-body">
                        <h5 class="card-title"><?php echo $row["nom"]; ?></h5>
                        <p class="card-text"><?php echo number_format($row["prix"], 2), "€"; ?></p>
                        </a>
                            <input type="hidden" name="nom" value="<?php echo $row["nom"]; ?>">
                            <input type="hidden" name="prix" value="<?php echo number_format($row["prix"], 2); ?>">
                            <input type="hidden" name="id" value="<?php echo $row["id"]; ?>">
                        <button type="submit" name="boutique" class="btn btn-info w-25" data-toggle="modal" data-target="#modalpanier"><i class="fas fa-cart-plus"></i></button>
                      </div>
                    </div>
                  </div>
              </div>
       </form>
    <?php
      }
        } else {
          ?>
          <h3 class="h2 f-jomalhari text-center text-secondary ml-auto mr-auto mt-5 pt-5"> <?php echo "Chocolats à venir"; ?></h3>
          <?php
        }
      }
?>


<?php
  }else{


    //Todos los productos

    $sql = "SELECT * FROM chocolats";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            ?>
        <form action="includes/panier.php" method="POST" style="width: 300px;">
          <a href="" class="vinc-produit">
            <div class="col-md-4 mt-5 ml-5 ml-md-2">
              <div class="item text-center m-2">
                  <div class="card card-saison" style="width: 15rem;">
                    <img src="<?php echo $row["image_chocolat"]; ?>" height="200px"  style="object-fit: cover;" class="card-img-top card-img-top producto-imagen gallery-image " alt="...">
                    <div class="card-body">
                      <h5 class="card-title"><?php echo $row["nom"]; ?></h5>
                      <p class="card-text"><?php echo number_format($row["prix"], 2), "€"; ?></p>
                      </a>
                          <input type="hidden" name="nom" value="<?php echo $row["nom"]; ?>">
                          <input type="hidden" name="prix" value="<?php echo number_format($row["prix"], 2); ?>">
                          <input type="hidden" name="id" value="<?php echo $row["id"]; ?>">
                      <button type="submit" name="boutique" class="btn btn-info w-25" data-toggle="modal" data-target="#modalpanier"><i class="fas fa-cart-plus"></i></button>
                    </div>
                  </div>
                </div>
            </div>
     </form>
      <?php
            }
        } else {

          ?>
          <h3 class="h2 f-jomalhari text-center text-secondary ml-auto mr-auto mt-5 pt-5"> <?php echo "Chocolats à venir"; ?></h3>

        <?php
          }
      }
      ?>










      </div>
    </div>

  </section>

<!-- Footer -->
<section id="footer" class="mt-5">
		<div class="container">
			<div class="row text-center text-xs-center text-sm-left text-md-left">
				<div class="col-xs-12 col-sm-4 col-md-4">
					<h5>Navegation</h5>
					<ul class="list-unstyled quick-links">
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Home</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Events</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>About</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Boutique</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					<h5>Boutique</h5>
					<ul class="list-unstyled quick-links">
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Chocolaterie</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Patisserie</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Glace</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Gourmandise</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					<h5>Informations</h5>
					<ul class="list-unstyled quick-links">
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Home</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>About</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>FAQ</a></li>
						<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Get Started</a></li>
						<li><a href="https://wwwe.sunlimetech.com" title="Design and developed by"><i class="fa fa-angle-double-right"></i>Imprint</a></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
					<ul class="list-unstyled list-inline social text-center">
						<li class="list-inline-item"><a href="javascript:void();"><i class="fab fa-facebook-square"></i></a></li>
						<li class="list-inline-item"><a href="javascript:void();"><i class="fab fa-instagram"></i></a></li>
            <li class="list-inline-item"><a href="javascript:void();"><i class="fab fa-whatsapp"></i></a></li>

						<li class="list-inline-item"><a href="javascript:void();" target="_blank"><i class="fa fa-envelope"></i></a></li>
					</ul>
				</div>
				</hr>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
					<p class="h6">&copy All right Reversed.<a class="text-green ml-2" href="https://www.sunlimetech.com" target="_blank">Chocolaterie</a></p>
				</div>
				</hr>
			</div>
		</div>
  </section>


  <?php if(isset($_GET["Ajoute"])){ ?>
  <script type="text/javascript">
    function redireccionar(){
    document.getElementById('id01').style.display='active';
    $("#id01").trigger("click");
  }
  setTimeout ("redireccionar()", 1000); //tiempo expresado en milisegundos
</script>
<?php  } ?>



<div id="myModal" class="modal fade" role="dialog">
  <div  class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Notification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      ajouté au panier avec succès
      </div>
      <div class="modal-footer">
        <button type="button" id="id01" class="btn btn-outline-secondary" data-dismiss="modal">Fermer</button>
        <a class="btn btn-outline-primary" href="panier.php">Voir panier</a>

      </div>
    </div>
  </div>
</div>
	<!-- ./Footer -->



  <!--SCRIPT-->
<script>
  /* Set the width of the sidebar to 250px (show it) */
function openNav() {
  document.getElementById("mySidepanel").style.width = "250px";
}
/* Set the width of the sidebar to 0 (hide it) */
function closeNav() {
  document.getElementById("mySidepanel").style.width = "0";
}
</script>

  <script src="js/vendor/modernizr-3.7.1.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
  <script src="js/plugins.js"></script>
  <script src="js/main.js"></script>
  <script src="js/wow.min.js"></script>
  <script>

    wow = new WOW(
        {
        boxClass:     'wow',      // default
        animateClass: 'animated', // default
        offset:       30,          // default
        mobile:       true,       // default
        live:         true        // default
      }
      )
      wow.init();
    </script>


  <!--Boostrap js-->
  <script src="js/bootstrap.min.js"></script>
  <!--OWL CAROUSEL -->
  <script src="js/owl.carousel.js"></script>
  <script>
  $('.owl-carousel').owlCarousel({
    center: true,
    loop:true,
    margin:6,
    nav:true,
    autoplay:true,
    autoplayTimeout:2000,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:4
        }
    }
});
  </script>


  <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
  <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('set','transport','beacon'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async></script>
</body>

</html>
