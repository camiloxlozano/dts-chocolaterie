<?php
session_start();
if ($_SESSION['user'] == null ||  $_SESSION['user'] == ''){
	header("Location: index.php");
	}else{
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Home</title>

	<!-- Normalize V8.0.1 -->
	<link rel="stylesheet" href="./css/normalize.css">

	<!-- Bootstrap V4.3 -->
	<link rel="stylesheet" href="./css/bootstrap.min.css">

	<!-- Bootstrap Material Design V4.0 -->
	<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">

	<!-- Font Awesome V5.9.0 -->
	<link rel="stylesheet" href="./css/all.css">

	<!-- Sweet Alerts V8.13.0 CSS file -->
	<link rel="stylesheet" href="./css/sweetalert2.min.css">

	<!-- Sweet Alert V8.13.0 JS file-->
	<script src="./js/sweetalert2.min.js" ></script>

	<!-- jQuery Custom Content Scroller V3.1.5 -->
	<link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">
	
	<!-- General Styles -->
	<link rel="stylesheet" href="./css/style.css">


</head>
<body>
	

	<!--Menu-->
	<?php
	include 'includes/menu.php'
	?>
		<!-- Page content -->
		<section class="full-box page-content">
			<nav class="full-box navbar-info">
				<a href="#" class="float-left show-nav-lateral">
					<i class="fas fa-exchange-alt"></i>
				</a>
				<a href="user-update.html">
					<i class="fas fa-user-cog"></i>
				</a>
				<a href="index.html" class="btn-exit-system">
					<i class="fas fa-power-off"></i>
				</a>
			</nav>

			<!-- Page header -->
			<div class="full-box page-header">
				<h3 class="text-left">
					<i class="fab fa-dashcube fa-fw"></i> &nbsp; DASHBOARD
				</h3>
				<p class="text-justify">
				Bienvenue sur le dashBoard, ici vous pouvez gérer tout votre site web
				</p>
			</div>
			
			<!-- Content -->
			<div class="full-box tile-container">

				<a href="evenement-list.php" class="tile">
					<div class="tile-tittle">Evenements</div>
					<div class="tile-icon">
						<i class="fas fa-users fa-fw"></i>
						<p> Evenements</p>
					</div>
				</a>

				<a href="produit-list.php" class="tile">
					<div class="tile-tittle">Produits</div>
					<div class="tile-icon">
						<i class="fas fa-pallet fa-fw"></i>
						<p> Produits</p>
					</div>
				</a>

				<a href="command-list.php" class="tile">
					<div class="tile-tittle">Commandes</div>
					<div class="tile-icon">
							<i class="fas fa-clipboard-list"></i>
						<p> Commandes</p>
					</div>
				</a>

				<a href="company.php" class="tile">
					<div class="tile-tittle">Entrprise</div>
					<div class="tile-icon">
						<i class="fas fa-store-alt fa-fw"></i>
						<p>configuration</p>
					</div>
				</a>
				
			</div>
			

		</section>
	</main>
	
	
	<!--=============================================
	=            Include JavaScript files           =
	==============================================-->
	<!-- jQuery V3.4.1 -->
	<script src="./js/jquery-3.4.1.min.js" ></script>

	<!-- popper -->
	<script src="./js/popper.min.js" ></script>

	<!-- Bootstrap V4.3 -->
	<script src="./js/bootstrap.min.js" ></script>

	<!-- jQuery Custom Content Scroller V3.1.5 -->
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>

	<!-- Bootstrap Material Design V4.0 -->
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>

	<script src="./js/main.js" ></script>
</body>
</html>