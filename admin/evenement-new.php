<?php
session_start();
if ($_SESSION['user'] == null ||  $_SESSION['user'] == ''){
	header("Location: index.php");
	}else{
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Nuevo cliente</title>

	<!-- Normalize V8.0.1 -->
	<link rel="stylesheet" href="./css/normalize.css">

	<!-- Bootstrap V4.3 -->
	<link rel="stylesheet" href="./css/bootstrap.min.css">

	<!-- Bootstrap Material Design V4.0 -->
	<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">

	<!-- Font Awesome V5.9.0 -->
	<link rel="stylesheet" href="./css/all.css">

	<!-- Sweet Alerts V8.13.0 CSS file -->
	<link rel="stylesheet" href="./css/sweetalert2.min.css">

	<!-- Sweet Alert V8.13.0 JS file-->
	<script src="./js/sweetalert2.min.js" ></script>

	<!-- jQuery Custom Content Scroller V3.1.5 -->
	<link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">
	
	<!-- General Styles -->
	<link rel="stylesheet" href="./css/style.css">


</head>
<body>
	
	<!-- Main container -->
	<main class="full-box main-container">

	<!--Menu-->
	<?php
	include 'includes/menu.php';
	?>
	<!-- Page content -->
		<section class="full-box page-content">
			<nav class="full-box navbar-info">
				<a href="#" class="float-left show-nav-lateral">
					<i class="fas fa-exchange-alt"></i>
				</a>
				<a href="user-update.html">
					<i class="fas fa-user-cog"></i>
				</a>
				<a href="#" class="btn-exit-system">
					<i class="fas fa-power-off"></i>
				</a>
			</nav>

			<!-- Page header -->
			<div class="full-box page-header">
				<h3 class="text-left">
					<i class="fas fa-plus fa-fw"></i> &nbsp; AJOUTEZ UN ÉVÉNEMENT
				</h3>
				<p class="text-justify">
				Sur cette page, vous pouvez ajouter un événement
				</p>
			</div>

			<div class="container-fluid">
				<ul class="full-box list-unstyled page-nav-tabs">
					<li>
						<a class="active" href="evenement-new.php"><i class="fas fa-plus fa-fw"></i> &nbsp; Ajouter un événement</a>
					</li>
					<li>
						<a href="evenement-list.php"><i class="fas fa-clipboard-list fa-fw"></i> &nbsp; liste des événements</a>
					</li>
				</ul>	
			</div>
			
			<!-- Content here-->
			<div class="container-fluid">
			<form action="../db/insert_evenement.php" method="POST" class="form-neon" autocomplete="off">
					<fieldset>
						<legend><i class="fas fa-users fa-fw"></i> &nbsp; Ajoutez des informations sur l'événement</legend>
						<div class="container-fluid">
							<div class="row">
								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="nom" class="bmd-label-floating">Nom de l'événement</label>
										<input type="text"  class="form-control" name="nom" id="nom" required>
									</div>
								</div>
								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="type" class="bmd-label-floating">Type</label>
										<select class="form-control" name="type" id="type">
											<option value="" selected="" disabled="">Sélectionnez une option</option>
											<option selected="" value="Cour de patisserie">Cour de patisserie</option>
											<option value="Brunch">Brunch</option>
											<option value="Salón de thé">Salón de thé</option>
										</select>
										  </datalist>
									</div>
								</div>

								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="image" class="bmd-label-static">Image</label>
										<input type="text" class="form-control" name="image" id="image" required>
									</div>
								</div>
								<div class="col-12 col-md-3">
									<div class="form-group">
										<label for="date" class="bmd-label-static">Date</label>
										<input type="date" class="form-control" name="date" id="date" required>
									</div>
								</div>
								<div class="col-12 col-md-3">
									<div class="form-group">
										<label for="houre" class="bmd-label-floating">Heure</label>
										<input type="text" class="form-control" name="houre" id="houre" required>
									</div>
								</div>
								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="prix" class="bmd-label-floating">Prix</label>
										<input type="text" pattern="[0-9.,]{1,20}" class="form-control" name="prix" id="prix" required>
									</div>
								</div>
								
								<div class="col-12 col-md-6">
										<div class="form-group">
											<label for="cupos" class="bmd-label-floating">Numero dePlaces</label>
											<input type="number" pattern="[0-9.,]{1,20}" class="form-control" name="cupos" id="cupos" required>
										</div>
									</div>
								<div class="col-12 col-md-12">
									<div class="form-group">
										<label for="contenido" class="bmd-label-floating">Description</label>
										<textarea class="form-control" name="contenido" id="contenido" rows="10" required></textarea>
									</div>
								</div>
							</div>
						</div>
					</fieldset>
					<br><br><br>
					<p class="text-center" style="margin-top: 40px;">
						<button type="reset" class="btn btn-raised btn-secondary btn-sm"><i class="fas fa-paint-roller"></i> &nbsp; effacer</button>
						&nbsp; &nbsp;
						<button type="submit" class="btn btn-raised btn-info btn-sm"><i class="far fa-save"></i> &nbsp; Sauvegarder</button>
					</p>
				</form>
			</div>	

		</section>
	</main>
	
	
	<!--=============================================
	=            Include JavaScript files           =
	==============================================-->
	<!-- jQuery V3.4.1 -->
	<script src="./js/jquery-3.4.1.min.js" ></script>

	<!-- popper -->
	<script src="./js/popper.min.js" ></script>

	<!-- Bootstrap V4.3 -->
	<script src="./js/bootstrap.min.js" ></script>

	<!-- jQuery Custom Content Scroller V3.1.5 -->
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>

	<!-- Bootstrap Material Design V4.0 -->
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>

	<script src="./js/main.js" ></script>
</body>
</html>