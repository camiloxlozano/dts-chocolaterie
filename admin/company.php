<?php
session_start();
include '../db/conn.php';
if ($_SESSION['user'] == null ||  $_SESSION['user'] == ''){
    header("Location: index.php");

	}else{
    }
    $sql = "SELECT * FROM enterprise WHERE id= 1";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();

    if ($result->num_rows > 0) {

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Donnes de l'entreprise</title>

    <!-- Normalize V8.0.1 -->
    <link rel="stylesheet" href="./css/normalize.css">

    <!-- Bootstrap V4.3 -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">

    <!-- Bootstrap Material Design V4.0 -->
    <link rel="stylesheet" href="./css/bootstrap-material-design.min.css">

    <!-- Font Awesome V5.9.0 -->
    <link rel="stylesheet" href="./css/all.css">

    <!-- Sweet Alerts V8.13.0 CSS file -->
    <link rel="stylesheet" href="./css/sweetalert2.min.css">

    <!-- Sweet Alert V8.13.0 JS file-->
    <script src="./js/sweetalert2.min.js"></script>

    <!-- jQuery Custom Content Scroller V3.1.5 -->
    <link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">

    <!-- General Styles -->
    <link rel="stylesheet" href="./css/style.css">


</head>


<body>
    <!-- Main container -->
    <main class="full-box main-container">
		<!--Menu-->
		<?php
		include 'includes/menu.php';
		?>

        <section class="full-box page-content">
            <nav class="full-box navbar-info">
                <a href="#" class="float-left show-nav-lateral">
                    <i class="fas fa-exchange-alt"></i>
                </a>
                <a href="user-update.html">
                    <i class="fas fa-user-cog"></i>
                </a>
                <a href="#" class="btn-exit-system">
                    <i class="fas fa-power-off"></i>
                </a>
            </nav>
            <!-- Page header -->
            <div class="full-box page-header">
                <h3 class="text-left">
                    <i class="fas fa-building fa-fw"></i> &nbsp; ENTREPRISE
                </h3>
                <p class="text-justify">
                ici ces informations de base de l'entreprise (Ici vous pouvez aussi éditer quelques textes qui apparaissent sur le site web)</p>
            </div>

            <!--CONTENT-->
            <div class="container-fluid">
                <form action="../db/modif_enterprise.php" method="POST" class="form-neon" autocomplete="off">
                    <fieldset>
                        <legend><i class="far fa-building"></i> &nbsp; textes</legend>
                        <div class="container-fluid">
                            <div class="row">
                                <p class="mt-3" style="color:gray;">(br pour rupture de ligne)</p>
                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="textprincipale" class="bmd-label-floating">Texte d'en-tête (60)</label>
                                        <input type="form" pattern="{1,60}" class="form-control" name="textprincipale" id="empresa_explicacion" value="<?php echo $row["textprincipale"];?>">
                                    </div>
                                </div>

                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="embleme" class="bmd-label-floating">Emblème de l'entreprise(60)</label>
                                        <input type="form" pattern="{1,60}" class="form-control" name="embleme" id="empresa_explicacion" value="<?php echo $row["embleme"];?>" >
                                    </div>
                                </div>

                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="subembleme" class="bmd-label-floating">Subemblème de l'entreprise(60)</label>
                                        <input type="form" pattern="{1,60}" class="form-control" name="subembleme" id="empresa_explicacion" value="<?php echo $row["subembleme"];?>">
                                    </div>
                                </div>

                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="textsaison" class="bmd-label-floating">texte des chocolats de saison(300)</label>
                                        <input type="form" pattern="{1,60}" class="form-control" name="textsaison" id="empresa_explicacion" value="<?php echo $row["textsaison"];?>">
                                    </div>
                                </div>


                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="empresa_explicacion" class="bmd-label-floating">À propos de nous</label>
                                        <textarea class="form-control" name="Apropos" id="produit-contenu" rows="10" ><?php echo $row["Apropos"];?></textarea>
                                    </div>
                                </div>






                                <legend class="my-3"></i> &nbsp; Images des categories</legend>

                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="img_glacerie" class="bmd-label-floating">Image de glacerie</label>
                                        <input type="form"  class="form-control" name="img_glacerie" id="empresa_explicacion" value="<?php echo $row["img_glacerie"];?>">
                                    </div>
                                </div>

                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="img_chocolaterie" class="bmd-label-floating">Image de chocolaterie</label>
                                        <input type="form"  class="form-control" name="img_chocolaterie" id="empresa_explicacion"value="<?php echo $row["img_chocolaterie"];?>">
                                    </div>
                                </div>

                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="img_gourmandise" class="bmd-label-floating">image de la gormandise</label>
                                        <input type="form" pattern="{1,60}" class="form-control" name="img_gourmandise" id="empresa_explicacion"value="<?php echo $row["img_patiserie"];?>">
                                    </div>
                                </div>

                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="img_patiserie" class="bmd-label-floating">image de la patisserie</label>
                                        <input type="form" pattern="{1,60}" class="form-control" name="img_patiserie" id="empresa_explicacion"value="<?php echo $row["img_gourmandise"];?>">
                                    </div>
                                </div>

                                <legend class="my-3"></i> &nbsp; Images de la gallerie</legend>

                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="img_galerie_1" class="bmd-label-floating">img_galerie_1</label>
                                        <input type="form" pattern="{1,60}" class="form-control" name="img_galerie_1" id="empresa_explicacion"value="<?php echo $row["img_galerie_1"];?>">
                                    </div>
                                </div>



                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="img_galerie_2" class="bmd-label-floating">img_galerie_2</label>
                                        <input type="form" pattern="{1,60}" class="form-control" name="img_galerie_2" id="empresa_explicacion"value="<?php echo $row["img_galerie_2"];?>">
                                    </div>
                                </div>

                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="img_galerie_3" class="bmd-label-floating">img_galerie_3</label>
                                        <input type="form" pattern="{1,60}" class="form-control" name="img_galerie_3" id="empresa_explicacion"value="<?php echo $row["img_galerie_3"];?>">
                                    </div>
                                </div>

                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="img_galerie_4" class="bmd-label-floating">img_galerie_4</label>
                                        <input type="form" pattern="{1,60}" class="form-control" name="img_galerie_4" id="empresa_explicacion"value="<?php echo $row["img_galerie_4"];?>">
                                    </div>
                                </div>

                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="img_galerie_5" class="bmd-label-floating">img_galerie_5</label>
                                        <input type="form" pattern="{1,60}" class="form-control" name="img_galerie_5" id="empresa_explicacion"value="<?php echo $row["img_galerie_5"];?>">
                                    </div>
                                </div>

                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="img_galerie_6" class="bmd-label-floating">img_galerie_6</label>
                                        <input type="form" pattern="{1,60}" class="form-control" name="img_galerie_6" id="empresa_explicacion"value="<?php echo $row["img_galerie_6"];?>">
                                    </div>
                                </div>

                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="img_galerie_7" class="bmd-label-floating">img_galerie_7</label>
                                        <input type="form" pattern="{1,60}" class="form-control" name="img_galerie_7" id="empresa_explicacion"value="<?php echo $row["img_galerie_7"];?>">
                                    </div>
                                </div>

                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="img_galerie_8" class="bmd-label-floating">img_galerie_8</label>
                                        <input type="form" pattern="{1,60}" class="form-control" name="img_galerie_8" id="empresa_explicacion"value="<?php echo $row["img_galerie_8"];?>">
                                    </div>
                                </div>

                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="img_galerie_9" class="bmd-label-floating">img_galerie_9</label>
                                        <input type="form" pattern="{1,60}" class="form-control" name="img_galerie_9" id="empresa_explicacion"value="<?php echo $row["img_galerie_9"];?>">
                                    </div>
                                </div>

                    </fieldset>
                    <br><br>
                    <p class="text-center" style="margin-top: 40px;">
                        <button type="reset" class="btn btn-raised btn-secondary btn-sm"><i class="fas fa-paint-roller"></i> &nbsp; EFFACER</button>
                        &nbsp; &nbsp;
                        <button type="submit" class="btn btn-raised btn-info btn-sm"><i class="far fa-save"></i> &nbsp; SAUVEGARDER</button>
                    </p>
                </form>
            </div>




        </section>
    </main>


    <!--=============================================
	=            Include JavaScript files           =
	==============================================-->
    <!-- jQuery V3.4.1 -->
    <script src="./js/jquery-3.4.1.min.js"></script>

    <!-- popper -->
    <script src="./js/popper.min.js"></script>

    <!-- Bootstrap V4.3 -->
    <script src="./js/bootstrap.min.js"></script>

    <!-- jQuery Custom Content Scroller V3.1.5 -->
    <script src="./js/jquery.mCustomScrollbar.concat.min.js"></script>

    <!-- Bootstrap Material Design V4.0 -->
    <script src="./js/bootstrap-material-design.min.js"></script>
    <script>
        $(document).ready(function() {
            $('body').bootstrapMaterialDesign();
        });
    </script>

    <script src="./js/main.js"></script>
</body>
</html>
<?php
    }
?>