<?php
include '../db/conn.php';

session_start();
if ($_SESSION['user'] == null ||  $_SESSION['user'] == ''){
	header("Location: index.php");
	}else{
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Lista de clientes</title>

	<!-- Normalize V8.0.1 -->
	<link rel="stylesheet" href="./css/normalize.css">

	<!-- Bootstrap V4.3 -->
	<link rel="stylesheet" href="./css/bootstrap.min.css">

	<!-- Bootstrap Material Design V4.0 -->
	<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">

	<!-- Font Awesome V5.9.0 -->
	<link rel="stylesheet" href="./css/all.css">

	<!-- Sweet Alerts V8.13.0 CSS file -->
	<link rel="stylesheet" href="./css/sweetalert2.min.css">

	<!-- Sweet Alert V8.13.0 JS file-->
	<script src="./js/sweetalert2.min.js" ></script>

	<!-- jQuery Custom Content Scroller V3.1.5 -->
	<link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">
	
	<!-- General Styles -->
	<link rel="stylesheet" href="./css/style.css">


</head>
<body>
	
	<!-- Main container -->
	<main class="full-box main-container">

	!--Menu-->
	<?php
	include 'includes/menu.php';
	?>

		<!-- Page content -->
		<section class="full-box page-content">
			<nav class="full-box navbar-info">
				<a href="#" class="float-left show-nav-lateral">
					<i class="fas fa-exchange-alt"></i>
				</a>
				<a href="evenement-update.php">
					<i class="fas fa-user-cog"></i>
				</a>
				<a href="#" class="btn-exit-system">
					<i class="fas fa-power-off"></i>
				</a>
			</nav>

			<!-- Page header -->
			<div class="full-box page-header">
				<h3 class="text-left">
					<i class="fas fa-clipboard-list fa-fw"></i> &nbsp; LISTE DES ÉVÉNEMENTS

				</h3>
				<p class="text-justify">
				Voici la liste des événements disponibles

				</p>
			</div>

			<div class="container-fluid">
				<ul class="full-box list-unstyled page-nav-tabs">
					<li>
						<a href="evenement-new.php"><i class="fas fa-plus fa-fw"></i> &nbsp; Ajouter un événement</a>
					</li>
					<li>
						<a class="active" href="evenement-list.php"><i class="fas fa-clipboard-list fa-fw"></i> &nbsp; liste des événements</a>
					</li>
				</ul>	
			</div>
			
			<!-- Content here-->
			<div class="container-fluid">
				<div class="table-responsive">
					<table class="table table-dark table-sm">
						<thead>
							<tr class="text-center roboto-medium">
								<th>#</th>
								<th>PLACES</th>
								<th>NOM</th>
								<th>TYPE</th>
								<th>ACTUALISER</th>
								<th>EFFACER</th>
							</tr>
						</thead>
						<tbody>


						<?php
            $sql = "SELECT * FROM evenement";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    ?>
							<tr class="text-center" >
								<td><?php echo $row["id"]; ?></td>
								<td><?php echo $row["places"]; ?></td>
								<td><?php echo $row["nom"]; ?></td>
								<td><?php echo $row["type_evenement"]; ?></td>
								<td>
									<a href="evenement-update.php?id=<?php echo $row["id"]; ?>" class="btn btn-success">
											<i class="fas fa-edit"></i>
									</a>
								</td>
								<td>

								<form action="../db/delete_evenement.php?id=<?php echo $row["id"]; ?>" method="POST">
										<button type="submit" name="update_list" class="btn btn-warning">
		  									<i class="far fa-trash-alt"></i>
										</button>
										</form>
								</td>
							</tr>


							<?php
                }
            } else {

			  ?>

	              <h3 class="h3 f-jomalhari text-center text-secondary ml-auto mr-auto mb-5"> <?php echo "No hay eventos disponibles"; ?></h3>
            <?php
              }

            ?>



						</tbody>
					</table>
				</div>

			</div>

			
<hr class="my-5">

<h2 class="text-center ">Tabal de inscritos</h2>



<div class="container-fluid">
				<div class="table-responsive">
					<table class="table table-dark table-sm">
						<thead>
							<tr class="text-center roboto-medium">
								<th>NOM</th>
								<th>EMAIL</th>
								<th>TELEPHONE</th>
								<th>NUMERO EVENEMENT</th>
								<th>EFFACER</th>
							</tr>
						</thead>
						<tbody>


						<?php
            $sql = "SELECT * FROM evenement_inscrip";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    ?>
							<tr class="text-center" >
								<td><?php echo $row["nom"]; ?></td>
								<td><?php echo $row["email"]; ?></td>
								<td><?php echo $row["telephone"]; ?></td>
								<td><?php echo $row["id_evenement"]; ?></td>
								<td>
								<form action="../db/delete_inscrip.php?id=<?php echo $row["id"];?>&id-even=<?php echo $row["id_evenement"];?>" method="POST">
										<button type="submit" name="update_list" class="btn btn-warning">
		  									<i class="far fa-trash-alt"></i>
										</button>
										</form>
								</td>
							</tr>
							<?php
                			}
            			} else {
			  				?>
	              				<h3 class="h3 f-jomalhari text-center text-secondary ml-auto mr-auto mb-5"> <?php echo "No hay suscriptores disponibles"; ?></h3>
            				<?php
              				}
            				?>
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</main>
	
	
	<!--=============================================
	=            Include JavaScript files           =
	==============================================-->
	<!-- jQuery V3.4.1 -->
	<script src="./js/jquery-3.4.1.min.js" ></script>

	<!-- popper -->
	<script src="./js/popper.min.js" ></script>

	<!-- Bootstrap V4.3 -->
	<script src="./js/bootstrap.min.js" ></script>

	<!-- jQuery Custom Content Scroller V3.1.5 -->
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>

	<!-- Bootstrap Material Design V4.0 -->
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>

	<script src="./js/main.js" ></script>
</body>
</html>